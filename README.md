# README #

### Scud Bomber 1.0b ###
![Scud.jpg](https://bitbucket.org/repo/z8j66xK/images/1139575001-Scud.jpg)

* Date: 24/08/2016
* Langage:FR
* Dev: Python
* BY: KURO

* Scud est un outil simple écrit en python à des fins éducatives, son principe est l'envoi massif de mail à une cible définie afin que la boîte soit totalement saturé.
* Cette outil montre comment python utilise le SMTP.

# Utilisation #

*  python Scud.py